# Input validation

For the forthcoming feature **input validation**,  you need to ensure that the input files (`predict.txt` and `solution.txt`) are correctly formatted and handle possible errors gracefully. Within your team, you can switch roles to diversify experience. Essentially, the steps remains the same to the feature `MSE`.

## Implement input validation

<details>
<summary>Solution</summary>
<p>

- Create a feature `b_input_validation` branch:

```bash
git pull origin develop
git checkout develop
git checkout -b b_input_validation
```

</p>

<p>

- `score.py`:

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
from sys import argv
import numpy as np
    

# ========= Useful functions ==============
def read_array(filename):
    ''' Read array and convert to 2d np arrays '''
    if not os.path.exists(filename):
        raise FileNotFoundError(f"The file {filename} does not exist.")
    
    formatted_array = []

    with open(filename, 'r') as file:
        for line_num, line in enumerate(file, start=1):
            # Split the line into elements and strip whitespace
            elements = line.strip().split()

            # Check if there are exactly three elements
            if len(elements) != 3:
                raise ValueError(f"Error in {filename}, line {line_num}: Expected 3 elements, found {len(elements)}")

            # Check if all elements are either '0' or '1'
            if not all(elem in ['0', '1'] for elem in elements):
                raise ValueError(f"Error in {filename}, line {line_num}: Elements must be '0' or '1'")

            # Convert elements to integers and add to the array
            formatted_array.append([int(elem) for elem in elements])

    # Convert the list to a numpy array
    return np.array(formatted_array)


def accuracy_metric(solution, prediction):
    if len(solution) == 0 or len(prediction) == 0:
        return 0
    correct_samples = np.all(solution == prediction, axis=1)
    return np.mean(correct_samples)


def mse_metric(solution, prediction):
    '''Mean-square error.
    Works even if the target matrix has more than one column'''
    if len(solution) == 0 or len(prediction) == 0:
        return 0
    mse = np.sum((solution - prediction)**2, axis=1)
    return np.mean(mse)


def _HERE(*args):
    h = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(h, *args)
    

# =============================== MAIN ========================================
if __name__ == "__main__":

    #### INPUT/OUTPUT: Get input and output directory names
    try:
        prediction_file = argv[1]
        solution_file = argv[2]
        # Read the solution and prediction values into numpy arrays
        solution = read_array(solution_file)
        prediction = read_array(prediction_file)
    except IndexError:
        print("Usage: script.py predict.txt solution.txt")
        sys.exit(1)
    except (FileNotFoundError, IOError) as e:
        print(e)
        sys.exit(1)
    
    score_file = open(_HERE('scores.txt'), 'w')
    # # Extract the dataset name from the file name
    prediction_name = os.path.basename(prediction_file)
    
    # Check if the shapes of the arrays are compatible
    if prediction.shape != solution.shape:
        print("Error: Prediction and solution arrays have different shapes.")
        sys.exit(1)
    
    # Compute the score prescribed by the metric file 
    accuracy_score = accuracy_metric(solution, prediction)
    mse_score = mse_metric(solution, prediction)
    print(
        "======= (" + prediction_name + "): score(accuracy_metric)=%0.2f =======" % accuracy_score)
    print(
        "======= (" + prediction_name + "): score(mse_metric)=%0.2f =======" % mse_score)
    # Write score corresponding to selected task and metric to the output file
    score_file.write("accuracy_metric: %0.2f\n" % accuracy_score)
    score_file.write("mse_metric: %0.2f\n" % mse_score)
    score_file.close()
```

</p>
</details>

## Implement unit tests for input validation

<details>
<summary>Solution</summary>
<p>

<p>

Create a file `tests/test_fileformat.py`:

```python
import unittest
import numpy as np
import os, sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from score import read_array

class TestFileFormat(unittest.TestCase):

    def setUp(self):
        """Create sample files for testing."""
        self.correct_format_file = 'correct_format.txt'
        self.wrong_format_file = 'wrong_format.txt'

        with open(self.correct_format_file, 'w') as f:
            f.write('0 1 0\n1 0 1\n0 0 0')

        with open(self.wrong_format_file, 'w') as f:
            f.write('0 1\n1 1 0 1\na b c')

    def tearDown(self):
        """Remove the created test files."""
        os.remove(self.correct_format_file)
        os.remove(self.wrong_format_file)

    def test_correct_format(self):
        """Test that correctly formatted files are read without errors."""
        result = read_array(self.correct_format_file)
        expected = np.array([[0, 1, 0], [1, 0, 1], [0, 0, 0]])
        np.testing.assert_array_equal(result, expected)

    def test_wrong_format(self):
        """Test that incorrectly formatted files raise an error."""
        with self.assertRaises(ValueError):
            read_array(self.wrong_format_file)


if __name__ == '__main__':
    unittest.main()
```

</p>
</details>

## Create a merge request

## Approve a merge request

## Merge `b_input_validation` into `develop`
